# README

# How to run the project
* cp .env .env.development
* Insert your DB data on .env.development file
* $ yarn install
* $ rails db:create
* $ rails db:migrate
* rails s

---

# Tools I have used (or at least tried) and didn't added to avoid unecessary dependencies

* [Rubocop](https://github.com/bbatsov/rubocop), trying to keep the project in the same *pattern*

---

# Nice things to talk about what I did

* I always avoid keep some database field without validation, and I've used in the couple years the `default:` and `null:` option when there is some required data. It helps me gets the `undefined method *** for nil class` in a lot of ways. (eg: when I have to truncate some string)

---
