# frozen_string_literal: true

Rails.application.routes.draw do
  namespace :api do
    namespace :v1 do
      resources :events, only: :index
    end
  end

  resources :events, only: [:index]

  root 'events#index'
end
