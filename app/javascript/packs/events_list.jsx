import React from 'react'
import ReactDOM from 'react-dom'
import PropTypes from 'prop-types'
import $ from 'jquery'

class EventsList extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      events: []
    };
  }

  loadEvents() {
    let items;
    $.getJSON('/api/v1/events.json', (response) => {
      this.setState({events: response})
    });
  }

  render() {
    this.loadEvents()
    return (
      <div>
        <ul>
          {
            this.state.events.map((e) => {
              return(
                <li>
                  {e.name}
                  {
                    <ul>
                      {
                        e.topics.map((t) => {
                          return(
                            <li>{t.name}</li>
                          )
                        })
                      }
                    </ul>
                  }
                </li>
              )
            })
          }
        </ul>
      </div>
    );
  }
};

/*
document.addEventListener('DOMContentLoaded', () => {
  ReactDOM.render(
    <EventsList />,
    document.getElementById('events-list'),
  )
})
*/
