# frozen_string_literal: true

module Api
  module V1
    class EventsController < ApplicationController
      def index
        render json: Event.includes(:topics).map { |event| event.attributes.merge(topics: event.topics.map(&:attributes)) }
      end
    end
  end
end
