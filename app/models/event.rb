# frozen_string_literal: true

class Event < ApplicationRecord
  has_many :topics, dependent: :destroy
  validates :name, :start_time, :end_time, :date, presence: true
end
