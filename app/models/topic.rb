# frozen_string_literal: true

class Topic < ApplicationRecord
  belongs_to :event

  validates :event, :name, presence: true
end
