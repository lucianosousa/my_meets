# frozen_string_literal: true

class CreateEvents < ActiveRecord::Migration[5.1]
  def change
    create_table :events do |t|
      t.string :name, null: false, default: ''
      t.time :start_time, null: false
      t.time :end_time, null: false
      t.date :date, null: false

      t.timestamps
    end
  end
end
