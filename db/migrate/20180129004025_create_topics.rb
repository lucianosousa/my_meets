# frozen_string_literal: true

class CreateTopics < ActiveRecord::Migration[5.1]
  def change
    create_table :topics do |t|
      t.string :name, null: false, default: ''
      t.references :event, foreign_key: true

      t.timestamps
    end
  end
end
