# frozen_string_literal: true

FactoryBot.define do
  factory :topic do
    name
    event
  end
end
