# frozen_string_literal: true

FactoryBot.define do
  factory :event do
    name
    start_time '2018-01-28 19:10:57'
    end_time '2018-01-28 19:10:57'
    date '2018-01-28'
  end
end
