# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Api::V1::EventsController, type: :controller do
  let!(:event_01) { FactoryBot.create(:event) }
  let!(:event_02) { FactoryBot.create(:event) }
  let!(:topic_01) { FactoryBot.create(:topic, event: event_01) }

  describe 'GET "index"' do
    before do
      get :index
    end

    it do
      expect(response).to be_successful
      body = JSON.parse(response.body)
      expect(body[0]['name']).to eq(event_01.name)
      expect(body[0]['topics'][0]['name']).to eq(topic_01.name)
      expect(body[1]['name']).to eq(event_02.name)
    end
  end
end
